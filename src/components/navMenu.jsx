import React from 'react';
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'

function NavMenu (){
    return(
        <div className="navbar-item">
            <a href="#"><i><FontAwesomeIcon icon={faEllipsisV} /></i></a>
        </div>
    );
}

export default NavMenu;