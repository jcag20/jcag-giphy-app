import React from 'react';
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

function LoginSingInBtn (props){
    return (
        <div className='logBtn'>
            <div className="iconLog">
                <a href="#"><i><FontAwesomeIcon icon={faUser} /></i></a>
            </div>

            <div className="textLog">
                <a href="#">{props.text}</a>
            </div>
        </div>
    );
}

export default LoginSingInBtn;