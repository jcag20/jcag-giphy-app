import React from 'react';
import ReactDOM from 'react-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

function InputSearch (props){
    return(
        <div className="finder">  
            <div className="search-text">
                <input type="text" placeholder={props.label} className="search-text-box"/>
            </div>
        
            <div className="search-btn">
                <a href="#" className="s-btn"><i><FontAwesomeIcon icon={faSearch} /></i></a>
            </div>
        </div>  
    );
}

export default InputSearch;