import React from 'react';


function NavItem (props){
    return(
        <div className="navbar-item">
            <a href="#">{props.linkName}</a>
        </div>
 );
    
}
export default NavItem

