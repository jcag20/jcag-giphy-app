import React from 'react';

function Gif (props){
    return(
        <div className="carrusel-item">
            <img className="imgGif" src={props.source} alt={props.source}/>

        </div>
    );
}

export default Gif;