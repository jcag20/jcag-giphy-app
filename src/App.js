import React, {useState} from 'react';
import glogo from './giphy-logo.svg';
import './App.css';
import Title from './components/components';
import NavItem from './components/navbar-item';
import NavMenu from './components/navMenu';
import ActBotton from './components/actionBottons';
import LoginSingInBtn from './components/login';
import InputSearch from './components/input';
import Gif from './components/gif';

function App() {
  return (

    <div className="container">

      <header>
          <img src={glogo} className="glogo" alt="logo" />
          <Title text="GIPHY"></Title>
            <div className="navbar">
              <NavItem linkName="Reactions" />
              <NavItem linkName="Entertaiment" />
              <NavItem linkName="Sports" />
              <NavItem linkName="Stickers" />
              <NavItem linkName="Artist" />
              <NavMenu />
            </div>
            <div className="actButtons">
                <ActBotton text='Upload' />
                <ActBotton text='Create' />
            </div>
            <div className="logArea">
              <LoginSingInBtn text='Log In' />
            </div>
        </header>

        <InputSearch label="Search all the GIFs and stickers" />


        <div className="gifList">
          <Gif source="https://media.giphy.com/media/tsX3YMWYzDPjAARfeg/source.gif" />
          <Gif source="https://media.giphy.com/media/14smAwp2uHM3Di/source.gif" />
          <Gif source="https://media.giphy.com/media/3o7abKhOpu0NwenH3O/source.gif" />
          <Gif source="https://media.giphy.com/media/TlK63EI7rtUu9IAyxTW/source.gif" />
          <Gif source="https://media.giphy.com/media/TK4lSWZNA1ACBDZizY/source.gif" />
          <Gif source="https://media.giphy.com/media/3o7abKhOpu0NwenH3O/source.gif" />
          <Gif source="https://media.giphy.com/media/TlK63EI7rtUu9IAyxTW/source.gif" />
          <Gif source="https://media.giphy.com/media/TK4lSWZNA1ACBDZizY/source.gif" />
        </div>




    </div>



  );
}

export default App;
